$(
    function () {
        //For table sortable
        var fixHelper = function (e, ui) {
            ui.children().each(
                function () {
                    $(this).width($(this).width());
                }
            );
            return ui;
        };

        $('.handleButton').mousedown(
            function () {
                $(this).css(
                    {
                        cursor: 'grabbing'
                    }
                );
            }
        );
        $('.handleButton').mouseup(
            function () {
                $(this).css(
                    {
                        cursor: 'grab'
                    }
                );
            }
        );


        $("#sortable tbody").sortable(
            {
                revert: true,
                helper: fixHelper,
                handle: '.handleButton',
                start: function (event, ui) {

                    var el = ui.item;
                    var oldPosition = el.startPos;
                    var newPosition = el.index();

                    ui.item.startPos = ui.item.index();
                },
                stop: function (event, ui) {

                    var el = ui.item;
                    var oldPosition = el.startPos;
                    var newPosition = el.index();

                    if (oldPosition !== newPosition) {

                        var data = $('.positionGallery').serializeArray();
                        data.push({name: "id", value: $(el).children('.sortId').html()});

                        var i = 1;
                        $('.sortId').each(
                            function () {
                                data.push({name: "orders[" + i + "]", value: $(this).html()});
                                $(this).parent('tr').children('.pos').children('.position').text(i);
                                i = i + 1;
                            }
                        );

                        request = $.ajax(
                            {
                                type: "POST",
                                cache: false,
                                url: pathPosition,
                                dataType: "json",
                                data: data,
                                timeout: 3000
                            }
                        );
                        request.done(
                            function () {
                            }
                        );
                        request.fail(
                            function (jqXHR, textStatus, errorThrown) {
                                alert(textStatus);
                                console.error("The following error occurred: " + textStatus, errorThrown);
                            }
                        );
                    }
                    else {
                    }
                }
            }
        );

    }
);
