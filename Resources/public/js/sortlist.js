$(
    function () {

        $.extend(
            $.tablesorter.characterEquivalents, {
                "ae": "\u00e6", // expanding characters æ Æ
                "AE": "\u00c6",
                "oe": "\u00f6\u0153", // œ Œ
                "OE": "\u00d6\u0152",
                "d": "\u00f0", // Eth (ð Ð)
                "D": "\u00d0",
                "a": "\u00e1\u00e0\u00e2\u00e3\u00e4", // áàâãä
                "A": "\u00c1\u00c0\u00c2\u00c3\u00c4", // ÁÀÂÃÄ
                "c": "\u00e7", // ç
                "C": "\u00c7", // Ç
                "e": "\u00e9\u00e8\u00ea\u00eb", // éèêë
                "E": "\u00c9\u00c8\u00ca\u00cb", // ÉÈÊË
                "i": "\u00ed\u00ec\u0130\u00ee\u00ef", // íìIîï
                "I": "\u00cd\u00cc\u0130\u00ce\u00cf", // ÍÌIÎÏ
                "o": "\u00f3\u00f2\u00f4\u00f5\u00f6", // óòôõö
                "O": "\u00d3\u00d2\u00d4\u00d5\u00d6", // ÓÒÔÕÖ
                "S": "\u00df", // ß
                "u": "\u00fa\u00f9\u00fb\u00fc", // úùûü
                "U": "\u00da\u00d9\u00db\u00dc" // ÚÙÛÜ
            }
        );


        $('.tablesorter').tablesorter(
            {
                sortLocaleCompare: true,
                ignoreCase: true,
                widthFixed: false,
                showProcessing: false,
                headerTemplate: '{content}',
                onRenderTemplate: null,
                onRenderHeader: function (index) {
                    $(this).find('div.tablesorter-header-inner').addClass('roundedCorners');
                },
                cancelSelection: true,
                dateFormat: "mmddyyyy",
                sortMultiSortKey: "shiftKey",
                sortResetKey: 'ctrlKey',
                usNumberFormat: true,
                delayInit: false,
                serverSideSorting: false,
                headers: {
                    0: {
                        sorter: "digit"
                    },
                    1: {
                        sorter: "text"
                    },
                    2: {
                        sorter: "text"
                    },
                    3: {
                        sorter: "text"
                    },
                    4: {
                        sorter: "text"
                    },
                    5: {
                        sorter: false
                    },
                    6: {
                        sorter: false
                    }
                },
                sortForce: null,
                sortList: [
                [0, 0],
                [1, 0],
                [2, 0],
                [3, 0],
                [4, 0]
                ]

            }
        );


        $('.tablesorterVideo').tablesorter(
            {
                sortLocaleCompare: true,
                ignoreCase: true,
                widthFixed: false,
                showProcessing: false,
                headerTemplate: '{content}',
                onRenderTemplate: null,
                onRenderHeader: function (index) {
                    $(this).find('div.tablesorter-header-inner').addClass('roundedCorners');
                },
                cancelSelection: true,
                sortMultiSortKey: "shiftKey",
                sortResetKey: 'ctrlKey',
                usNumberFormat: true,
                delayInit: false,
                serverSideSorting: false,
                headers: {
                    0: {
                        sorter: "digit"
                    },
                    1: {
                        sorter: "text"
                    },
                    2: {
                        sorter: "text"
                    },
                    3: {
                        sorter: "text"
                    },
                    4: {
                        sorter: false
                    }
                },
                sortForce: null,
                sortList: [
                [0, 0],
                [1, 0],
                [2, 0],
                [3, 0]
                ]

            }
        );
    }
);
